function search() {
	var name = document.getElementById("artistname")
	var url = 'https://itunes.apple.com/search?term=' + name.value;
	var xhr = new XMLHttpRequest();
	xhr.open('GET', url, true);
	// xhr.setRequestHeader("Header-Custom-TizenCORS", "OK");
	xhr.setRequestHeader('Access-Control-Allow-Origin', "*");
	xhr.setRequestHeader('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
	xhr.setRequestHeader('Access-Control-Allow-Headers', 'Content-Type');
	xhr.onerror = function() {
		alert('Oops, there was an error making the request. \nAdd the CORS extension for the browser that you are using');
	};

	xhr.send();

	xhr.onload = function() {
		debugger;
		var text = xhr.responseText;
		var json = JSON.parse(text);
		var results = json.results;
		window.results = results;
		if (results.length <= 0) {
			$('#searchResults').html("NO RESULTS FOUND.");
			return;
		}

		var contentTable = $("<table/>", {
			id : "contentTable"
		});
		$('#searchResults').html(contentTable);
		var headerRow = $("<tr/>", {
			id : "headerRow"
		});
		var artistNameHeaderCell = $("<td/>", {
			class : "headerCell",
			id : "artistNameHeader",
			style : "width:150px; height:20px;",
			text : "Artist Name"
		});
		var releaseDateHeaderCell = $("<td/>", {
			class : "headerCell",
			id : "releaseDateHeader",
			text : "Release Date"
		});
		var trackNameHeaderCell = $("<td/>", {
			class : "headerCell",
			id : "trackNameHeader",
			text : "Track Name"
		});
		debugger;
		$(artistNameHeaderCell).appendTo($(headerRow));
		$(releaseDateHeaderCell).appendTo($(headerRow));
		$(trackNameHeaderCell).appendTo($(headerRow));
		$(headerRow).appendTo($(contentTable));
		debugger;
		for (var i = 0; i < results.length; i++) {
			var result = results[i];
			var newRow = $("<tr/>", {
				id : "tableRow" + i,
				style : "cursor:pointer;"
			});
			var emptyCell = $("<td/>", {
				class : "emptyCell",
				text : " "
			});

			var artistNameCell, releaseDateCell, trackNameCell = emptyCell;

			if (result.hasOwnProperty("artistName")) {
				artistNameCell = $("<td/>", {
					class : "artistNameCell",
					id : "artistNameCell" + i,
					text : result.artistName
				});
			}
			if (result.hasOwnProperty("releaseDate")) {
				releaseDateCell = $("<td/>", {
					class : "releaseDateCell",
					id : "releaseDateCell" + i,
					text : result.releaseDate.split('T')[0]
				});
			}
			if (result.hasOwnProperty("trackName")) {
				trackNameCell = $("<td/>", {
					class : "trackNameCell",
					id : "trackNameCell" + i,
					text : result.trackName
				});
			}
			$(artistNameCell).appendTo($(newRow));
			$(releaseDateCell).appendTo($(newRow));
			$(trackNameCell).appendTo($(newRow));
			$(newRow).appendTo($(contentTable));

			newRow[0].onclick = function(row) {
				openTrackDetails(row.currentTarget.rowIndex - 1);
			};
		}
		var anchorDiv = $("<a/>",{
			id : "downloadAnchorElement",
			text : "Download"
		});
		$(anchorDiv).appendTo($('#searchResults'));
		var dataStr = "data:text/json;charset=utf-8,"+ encodeURIComponent(JSON.stringify(json));
		var dlAnchorElem = document.getElementById('downloadAnchorElement');
		dlAnchorElem.setAttribute("href",dataStr);
		dlAnchorElem.setAttribute("download","tracks.txt");
		
		$(".headerCell").css({
			"width" : "150px",
			"height" : "20px",
			"font-weight" : "bold",
			"text-align" : "center",
			"border" : "1px solid #000000"
		});
		$("td").css({
			"width" : "150px",
			"height" : "30px",
			"text-align" : "left",
			"padding-left" : "10px",
			"border" : "1px solid #000000"
		});
		$("#contentTable").css({
			"border" : "1px solid #000000",
			"border-collapse" : "collapse"
		});

	};

	openTrackDetails = function(rowNum) {
		debugger;
		var artistName, trackName, releaseDate, collectionName, collectionPrice, trackPrice, trackNumber, trackCount, image, kind, primaryGenreName = "";

		var result = this.results[rowNum];

		if (result.hasOwnProperty("artistName")) {
			artistName = result.artistName;
		}
		if (result.hasOwnProperty("trackName")) {
			trackName = result.trackName;
		}
		if (result.hasOwnProperty("releaseDate")) {
			var releaseDateString = result.releaseDate;
			releaseDate = releaseDateString.split('T')[0];
		}
		if (result.hasOwnProperty("collectionName")) {
			collectionName = result.collectionName;
		}
		if (result.hasOwnProperty("collectionPrice")) {
			collectionPrice = result.collectionPrice;
		}
		if (result.hasOwnProperty("trackPrice")) {
			trackPrice = result.trackPrice;
		}
		if (result.hasOwnProperty("trackNumber")) {
			trackNumber = result.trackNumber;
		}
		if (result.hasOwnProperty("trackCount")) {
			trackCount = result.trackCount;
		}
		if (result.hasOwnProperty("artworkUrl100")) {
			image = result.artworkUrl100;
		}
		if (result.hasOwnProperty("kind")) {
			kind = result.kind;
		}
		if (result.hasOwnProperty("primaryGenreName")) {
			primaryGenreName = result.primaryGenreName;
		}

		var trackDetailsDiv = $(
				"<div/>",
				{
					class : "trackDetailsDiv",
					id : "trackDetailsDiv",
					style : "width:40%; float:left;height:500px;border:1px solid #000000; "
				});
		var trackImage = $("<div/>", {
			class : "trackImage",
			id : "trackImage",
			style : "text-align:center; padding-top:20px;"
		});
		var trackSpecs = $("<div/>", {
			class : "trackSpecs",
			id : "trackSpecs",
			style : "text-align:center; padding-top:20px;"
		});
		var heading = $("<div/>", {
			id : "trackDetailsHeading",
			style : "text-align:center; padding-top:20px;"
		});
		$(heading).appendTo($(trackDetailsDiv));
		$(trackImage).appendTo($(trackDetailsDiv));
		$(trackSpecs).appendTo($(trackDetailsDiv));
		$('#trackDetails').html(trackDetailsDiv);
		$('#trackDetailsHeading').html('<h1>Track Details</h1>');
		$('#trackImage').html('<img src=' + image + '>');
		$('#trackSpecs').html(
				'Artist Name : ' + artistName + '<br>' + 'Track Name : '
						+ trackName + '<br>' + 'Release Date : ' + releaseDate
						+ '<br>' + 'Collection Name : ' + collectionName
						+ '<br>' + 'Collection Price : ' + collectionPrice
						+ '<br>' + 'Track Price : ' + trackPrice + '<br>'
						+ 'Track Number : ' + trackNumber + '<br>'
						+ 'Track Count : ' + trackCount + '<br>' + 'Kind : '
						+ kind + '<br>' + 'Genre : ' + primaryGenreName
						+ '<br>');

		window.scrollTo(0, 0);

	};

}