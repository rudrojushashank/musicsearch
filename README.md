# README #

This is a web application which uses Apple API to search music results of an artist.

* Open the index.html in a web browser.

* Enter the artist name and click on Search.

* If an alert box saying 'Oops, there was an error making the request. Add the CORS extension for the browser that you are using' pops up, download the CORS extension and add it to the browser.
The cors extension for chrome can be found at :

https://chrome.google.com/webstore/detail/allow-control-allow-origi/nlfbmbojpeacfghkpbjhddihlkkiljbi?hl=en 

* After adding the extension run the index.html, Enter the artist name and click on Search.

* Results are populated in a table with the details of Artist Name, Release Date, and Track Name.

* If you need more information about any particular track from the results click on the track row and a table with the complete details about the track such as  Artist Name, Track Name, Release Date, Collection Name, Collection Price, Track Price, Track Number, Track Count, Image, Kind and Primary Genre Name are displayed.